# Node Linux Image Generation

A toolchain to create auto-provisioning Linux images for the "LabCloud" manufacturing platform.

## Usage

To provision a new node, download the OS image by clicking [here](https://gitlab.com/sdu-labcloud/labcloud-node-linux-image-generation/-/jobs/artifacts/master/raw/raspbian-buster-lite-usb-provisioning-lite.img?job=raspbian) and flashing it onto an SD card.

On a USB stick, create an `install.ini` file with the following content:

```ini
# The name of your WiFi network.
SSID=MyWiFiNetwork
# The password to your WiFi network. Leave empty if your network requires no password.
PSK=MyWiFiPassword
# This can be either WPA-PSK or NONE. Set to NONE if your network has no password.
KEY_MGMT=WPA-PSK
# The URL to the provisioning script, you would like to execute, once the device is connected to the internet.
SCRIPT_URL=https://example.com/install.sh
```

You can also create a `environment.ini` file. This may contain any configuration or content that your application may need to run. It will be placed at `/tmp/usb-provisioning/environment.ini` and can be copied by the installation script provided via the `SCRIPT_URL` to any location.

Afterwards, start the device and wait until the LED blinks in a regular interval. Now insert the USB stick. The device will copy the files, connect to the internet and start blinking in a regular interval again. After disconnecting the USB stick, the device will execute the provisioning script.

## Build

This repository is meant to build the images fully-automated without any human interaction. This is done by use of the CI / CD features of GitLab. To have a look at the script that creates the modified image, head over to the `.gitlab-ci.yml` file. It contains a build task for each distribution that images are built upon. Currently, the following images are supported:

- [**raspbian-lite**](https://www.raspberrypi.org/downloads/raspbian/) by use of [**pi-gen**](https://github.com/RPi-Distro/pi-gen)

## Troubleshooting

1. **Missing `binfmt_misc` kernel module causes build to fail**  
   This is indicated by the following error message:

   ```txt
   update-binfmts: warning: Couldn't load the binfmt_misc module.
   ```

   If the build fails, check if the `binfmt_misc` kernel module is loaded on the Docker host, that is running the GitLab runner. This can be done by running:

   ```bash
   $ grep "binfmt_misc" /proc/modules
   binfmt_misc 20480 0 - Live 0x0000000000000000
   ```

   If there is no output or the output does not look similar, run:

   ```bash
   $ sudo modprobe binfmt_misc
   ```

## License

This project is licensed under the terms of the [MIT license](https://mit-license.org/).
